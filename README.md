# Java Optional | How to use optional in Java? IsEmpty? IfPresent? orElse?

[![Java Optional | How to use optional in Java? IsEmpty? IfPresent? orElse?](https://youtu.be/RRiccbxeJnE/0.jpg)](https://youtu.be/RRiccbxeJnE)

*__Kindly Like, Subscribe and Share the YouTube Video__*

## How to use optional in Java?

*Feel free to comment and let us know what kind video you would like to watch*

0. Initialize

```
Optional<String> optionalValue = Optional.empty();
Optional<String> optionalValue2 = Optional.of("Value");
```

1. IsEmpty

```
System.out.println("IsEmpty?: " + optionalValue.isEmpty());
if (optionalValue.isEmpty()) {
    // To something here....
}
```

2. IsPresent

```
System.out.println("IsPresent?: " + optionalValue2.isPresent());
if (optionalValue2.isPresent()) {
    // To something here ....
}
```

3. OrElse & OrElseGet

```
String value = optionalValue.orElse("Welcome to Live code");
String value2 = optionalValue
        .orElseGet(() -> "Welcome to Live Code!!!");
```

4. OrElseThrow

```
String value3 = optionalValue.orElseThrow(() -> new RuntimeException("Customize msg!!"));
```


5. IfPresent & IfPresentOrElse
```
optionalValue2
        .ifPresent(val -> System.out.println("IfPresent: " + val));
optionalValue.ifPresentOrElse(
        val -> System.out.println("Value is present: " + val),
        () -> System.out.println("Value is not present.....")
        );
```

6. Map
```
int valueLength = optionalValue2.map(val -> val.length()).orElse(0);
System.out.println("Value length: " + valueLength);
```


