package com.livecode;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {

        // Create a optional string called optionalValue and optionalValue2
        Optional<String> optionalValue = Optional.empty();
        Optional<String> optionalValue2 = Optional.of("Value");

        // Check whether Optional is empty?
        System.out.println("IsEmpty?: " + optionalValue.isEmpty());
        if (optionalValue.isEmpty()) {
            // To something here....
        }

        // Check whether Optional has value
        System.out.println("IsPresent?: " + optionalValue2.isPresent());
        if (optionalValue2.isPresent()) {
            // To something here ....
        }

        // Get the value inside optional
        String value = optionalValue.orElse("Welcome to Live code");
        String value2 = optionalValue
                .orElseGet(() -> "Welcome to Live Code!!!");
        System.out.println("Optional value: " + value);
        System.out.println("Optional value 2: " + value2);

        // Throw exception if optional value is not present
//        String value3 = optionalValue
//                .orElseThrow(() -> new RuntimeException("Customize msg!!"));

        // Perform action if Value is present or not present
        optionalValue2
                .ifPresent(val -> System.out.println("IfPresent: " + val));
        optionalValue.ifPresentOrElse(
                val -> System.out.println("Value is present: " + val),
                () -> System.out.println("Value is not present.....")
                );

        // Transform value to other type based on business logic
        int valueLength = optionalValue2.map(val -> val.length()).orElse(0);
        System.out.println("Value length: " + valueLength);
    }
}
